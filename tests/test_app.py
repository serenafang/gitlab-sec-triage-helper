
from gl_sec_triage_helper.app import Application


def test_app_constants():

    app = Application()

    assert app._fedramp_dr_label_prefix == 'FedRAMP::DR Status::', \
        'Should have correct "FedRAMP::DR Status::" label prefix'


def test_check_issue_labels():
    """Test the check_issue_labels function."""
    # Create a fake issue
    issue = {
        'labels': [
            'bug',
            'enhancement',
            'duplicate',
        ]
    }

    app = Application()

    assert app._check_issue_labels(issue) is True

    assert app._check_issue_labels(issue, exclude_labels=['duplicate']) is False
    assert app._check_issue_labels(issue, exclude_labels=['xyzabc']) is True
    assert app._check_issue_labels(issue, exclude_labels=['dup'], prefixes=True) is False
    assert app._check_issue_labels(issue, exclude_labels=['xyz'], prefixes=True) is True

    assert app._check_issue_labels(issue, include_labels=['duplicate']) is True
    assert app._check_issue_labels(issue, include_labels=['xyzabc']) is False
    assert app._check_issue_labels(issue, include_labels=['dup'], prefixes=True) is True
    assert app._check_issue_labels(issue, include_labels=['xyz'], prefixes=True) is False

    assert app._check_issue_labels(
        issue,
        exclude_labels=['xyzabc'],
        include_labels=['bug']) is True
    assert app._check_issue_labels(
        issue,
        exclude_labels=['duplicate'],
        include_labels=['bug']) is False


def test_labels_find_related():

    app = Application()

    assert app._labels_find_related(['A::B::C'], 'A::B::D') == 'A::B::C', \
        'Should find related label "A::B::C" for "A::B::D"'
    assert app._labels_find_related(['A::B::C'], 'A::B::C') == 'A::B::C', \
        'Should find related label "A::B::C" for "A::B::C"'
    assert app._labels_find_related(['A::B::C::D'], 'A::B::D') is None, \
        'Should find related label "A::B::C" for "A::B::C::D"'
    assert app._labels_find_related(['A::B::C::D', 'A::B::C'], 'A::B::D') == 'A::B::C', \
        'Should find related label "A::B::C" for "A::B::D"'


def test_calculate_labels():

    app = Application()

    assert app._calculate_labels(['A::B::C'], None, None) == ['A::B::C'], \
        'Should calculate labels ["A::B::C"]'
    assert app._calculate_labels(['A::B::C'], ['A::B::D'], None) == ['A::B::D'], \
        'Should calculate labels ["A::B::D"]'
    assert app._calculate_labels(['A::B::C'], ['A::B::D'], ['A::B::C']) == ['A::B::D'], \
        'Should calculate labels ["A::B::D"]'
    assert app._calculate_labels(['A::B::C'], ['A::B::D'], ['A::B::C', 'A::B::D']) == ['A::B::D'], \
        'Should calculate labels ["A::B::D"]'
    assert app._calculate_labels(['A', 'B', 'C', 'D'], None, ['B', 'C']) == ['A', 'D'], \
        'Should calculate labels ["A", "D"]'
    assert app._calculate_labels(['A', 'B', 'C', 'D'], ['F', 'H'], None) == ['A', 'B', 'C', 'D', 'F', 'H'], \
        'Should calculate labels ["A", "B", "C", "D", "F", "H"]'
