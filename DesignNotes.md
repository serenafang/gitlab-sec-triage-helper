
## TODO:

* Guards for only operating on fips image vulnerabilities

## Deviation Request Process

Templates:

Located at: `.gitlab/issue_templates`

* `vendor_dependency_template.md`
  * Can wait for label `Remediation SLO::Breaching SLO` is applied.
  * Not for CRIT/HIGH, do a risk adjustment with `FedRAMP Vendor Dependency` label
  * Checklist:
    * Provide URL for vulnerability details (CVE from vulnerability report)
    * Link vulnerability issue, issue should have `workflow::verification` and `FedRAMP DR Status::Open`
    * Provide a justification/explanation
    * Evidence attached in issue (??)
    * When ready for Security review, submit, labels auto-generated
    * GitLab is required by FedRAMP to follow up with the vendor/3rd party or open source project maintainers every 30 days
  * Data needed:
    * Check required boxes
    * CVE-xyz in abc
    * Container image + sha256
    * CVSSv3 (Severity)
    * Date detected
* `false_positive_template.md`
* `operational_requirement_template.md`
* `risk_adjustment_template.md`

Steps:

1. Get all issues in the project, look for `Remediation SLO::Breaching SLO`
1. Check the vulnerability severity using NVD CVE database
1. If `High/Crit` add a comment with at (@) mentions and message
   1. `Breaching SLO for {{ severity }} vulnerability.`
1. If MEDIUM, LOW file an Operational Requirement issue
   1. Get vulnerability URL from vulnerability issue
      `Issue created from vulnerability [62812055](https://gitlab.com/gitlab-org/security-products/analyzers/api-fuzzing-src/-/security/vulnerabilities/62812055)`
   1. Get image name + sha256
   1. Detected date == vulnerability report json `created_at`

## Design Notes

- Vulnerability class to encapsulate getting/working with

## Process automation

- Container Scan base-fips
- Container Scan production-fips
- Compare results and notify on fixes
  - Kick off a new master build?
  - Slack notification
