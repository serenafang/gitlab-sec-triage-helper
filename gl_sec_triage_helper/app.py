
import logging
import jinja2
import os
import requests
import urllib.parse
from .queries import Queries, VulnerabilityState, VulnerabilityReportType, VulnerabilitySeverity
from typing import Any, List, Dict, Optional


class ArgumentException(Exception):
    pass


class IssueCreationException(Exception):
    pass


class InvalidGitLabIdException(Exception):
    pass


class GraphQlErrorException(Exception):
    pass


class Application(object):
    ci_server_url: str
    ci_project_id: str
    ci_project_path: str
    dry_run: bool
    _fedramp_dr_label_prefix: str = 'FedRAMP::DR Status::'
    filter_image: Optional[str]
    jinja_env: jinja2.Environment
    _logger: logging.Logger
    token: str
    verbose: bool

    def __init__(self) -> None:
        self.filter_image = None
        self.dry_run = False
        self.verbose = False
        self._logger = logging.getLogger(__name__)
        self.jinja_env = jinja2.Environment(
            loader=jinja2.FileSystemLoader(f"{os.path.dirname(__file__)}/templates/")
        )

    def initialize(self) -> None:
        """Call once after arguments have been provided
        """
        self.graphql_api_endpoint = self._urljoin(self.ci_server_url, 'api/graphql')
        self._logger.info(self.graphql_api_endpoint)

    def _urljoin(self, *args: Any) -> str:
        """Join URL path segments. First argument can be a URL fragment.

        The arguments must be convertable to strings. Segments should not start/end with '/'.

        Returns:
            str: Joined url
        """

        join_args = []
        for arg in args:
            join_args.append(str(arg))

        return '/'.join(join_args)

    def _default_headers(self) -> Dict[str, Any]:
        """Return the default HTTP headers dict

        Returns:
            Dict[str, Any]: Default headers
        """
        return {'Authorization': f"Bearer {self.token}"}

    def _get_vulnerabilities(
            self,
            projectPath: str,
            states: List[VulnerabilityState],
            reportTypes: List[VulnerabilityReportType],
            severities: List[VulnerabilitySeverity],
            filter_image: str,
            resolvedOnDefaultBranch: Optional[bool] = None,
    ) -> List[Dict[str, Any]]:
        """Get a list of vulnerabilities in projectPath project.

        Args:
            projectPath (str): Project path (gitlab-org/security-products/analyzers/api-fuzzing-src)
            states (List[VulnerabilityState]): List of states to filter on
            reportTypes (List[VulnerabilityReportType]): List of report types to filter on
            severities (List[VulnerabilitySeverity]): List of severities to filter on
            filter_image (str): Filter by location.image.endwith(filter_image)
            resolvedOnDefaultBranch (Optional[bool]): Filter by resolvedOnDefaultBranch

        Returns:
            List[Dict[str, Any]]: List of vulnerability dicts
        """

        if not projectPath:
            raise ArgumentException('Error, _get_vulnerabilities requires a projectPath argument.')
        if not filter_image:
            raise ArgumentException('Error, _get_vulnerabilities requires a filter_image argument.')

        page_size = 100
        cursor = ""
        hasNextPage = True

        vulnerabilities = []

        while hasNextPage:
            variables = {
                'query': Queries.ProjectVulnerabilityQuery,
                'variables': {
                    'fullPath': projectPath,
                    'after': cursor,
                    'first': page_size,
                    'state': states,
                    'reportType': reportTypes,
                    'severity': severities,
                }
            }

            res = requests.post(
                url=self.graphql_api_endpoint,
                headers=self._default_headers(),
                json=variables
            )

            res.raise_for_status()
            data = res.json()

            if 'errors' in data:
                raise GraphQlErrorException(str(data['errors']))

            data = data['data']

            if data['project'] is None:
                return []

            vulnerabilities.extend(data['project']['vulnerabilities']['nodes'])

            pageInfo = data['project']['vulnerabilities']['pageInfo']
            hasNextPage = pageInfo['hasNextPage']
            cursor = pageInfo['endCursor']

        # If we don't have any filters then return current list
        if resolvedOnDefaultBranch is None and filter_image is None:
            return vulnerabilities

        ret = []
        for vuln in vulnerabilities:
            # self._logger.debug(f"vuln: resolvedOnDefaultBranch: {vuln['resolvedOnDefaultBranch']}")
            # self._logger.debug(f"vuln: location: {vuln['location']}")

            if (resolvedOnDefaultBranch is None or vuln['resolvedOnDefaultBranch']) \
               and (filter_image is None or str(vuln['location']['image']).endswith(filter_image)):

                ret.append(vuln)

        return ret

    def vulnerability_id_from_gid(self, gid: str) -> int:
        """Convert a gid vulnerability id into a integer id

        Args:
            gid (str): GID vulnerability ID (gid://gitlab/Vulnerability/1)

        Raises:
            InvalidGitLabIdException: Raised if the gid doesn't look valid

        Returns:
            int: Integer vulnerability id
        """

        if not gid.startswith('gid'):
            raise InvalidGitLabIdException("GitLab ID not prefixed with 'gid'")

        rindexSlash = gid.rindex('/')

        return int(gid[rindexSlash + 1:])

    def _vulnerability_update_state(self, serverUrl: str, gid: str, state: VulnerabilityState) -> None:
        """Update vulnerability state

        Args:
            serverUrl (str): Server URL (https://gitlab.com)
            gid (str): GitLab ID of the vulnerability (gid://gitlab/Vulnerability/1)
            state (VulnerabilityState): State to change to
        """

        state_map = {
            VulnerabilityState.CONFIRMED: 'confirm',
            VulnerabilityState.DETECTED: 'revert',
            VulnerabilityState.DISMISSED: 'dismiss',
            VulnerabilityState.RESOLVED: 'resolve',
        }

        vulnId = self.vulnerability_id_from_gid(gid)

        # Check current state of vulnerability
        url = self._urljoin(serverUrl, 'api/v4/vulnerabilities', str(vulnId))
        res = requests.get(url, headers=self._default_headers())
        res.raise_for_status()
        data = res.json()

        # Return if our state is correct already
        if data['state'] == str(state).lower():
            return

        # Change vulnerability state
        url = self._urljoin(serverUrl, 'api/v4/vulnerabilities', vulnId, state_map[state])
        res = requests.post(url, headers=self._default_headers())
        res.raise_for_status()

    def _issue_id_from_weburl(self, serverUrl: str, projectId: int, issueWebUrl: str) -> int:
        """Get an issue ID from issue web URL

        Args:
            serverUrl (str): Server URL (https://gitlab.com)
            projectId (int): Project that issue exists in
            issueWebUrl (str): Issue Web URL (https://gitlab.com/gitlab-org/security-products/analyzers/api-fuzzing-src/-/issues/55)

        Returns:
            int: Issue ID
        """

        url = self._issue_rest_url_from_weburl(serverUrl, projectId, issueWebUrl)
        res = requests.get(url, headers=self._default_headers())
        res.raise_for_status()

        data = res.json()

        return int(data['id'])

    def _issue_iid_from_weburl(self, issueWebUrl: str) -> int:
        """Return the issue id from an issue web url

        Args:
            issueWebUrl (str): Issue Web URL (https://gitlab.com/gitlab-org/security-products/analyzers/api-fuzzing-src/-/issues/55)

        Returns:
            int: Issue id
        """

        rindexSlash = issueWebUrl.rindex('/')
        rindexQuestion = issueWebUrl.rfind('?')

        if rindexQuestion > -1:
            issueId = int(issueWebUrl[rindexSlash + 1:rindexQuestion])
        else:
            issueId = int(issueWebUrl[rindexSlash + 1:])

        return int(issueId)

    def _vulnerability_id_from_gid(self, vuln_gid: str) -> int:
        """Return the vulnerability id from gid

        Args:
            vuln_gid (str): Vulnerability GitLab ID (gid://gitlab/Vulnerability/62614964)

        Returns:
            int: Vulnerability ID
        """

        rindexSlash = vuln_gid.rindex('/')
        rindexQuestion = vuln_gid.rfind('?')

        if rindexQuestion > -1:
            issueId = int(vuln_gid[rindexSlash + 1:rindexQuestion])
        else:
            issueId = int(vuln_gid[rindexSlash + 1:])

        return int(issueId)

    def _issue_rest_url_from_weburl(self, serverUrl: str, projectId: int, issueWebUrl: str) -> str:
        """Get an issue REST API url from an issue web URL

        Args:
            serverUrl (str): Server URL (https://gitlab.com)
            projectId (int): Project that issue exists in
            issueWebUrl (str): Issue Web URL (https://gitlab.com/gitlab-org/security-products/analyzers/api-fuzzing-src/-/issues/55)

        Returns:
            str: Issue REST API URL (https://gitlab.example.com/api/v4/projects/4/issues/85)
        """

        issueId = str(self._issue_iid_from_weburl(issueWebUrl))
        return self._urljoin(serverUrl, 'api/v4/projects', projectId, 'issues', issueId)

    def _issue_unlink(self, serverUrl: str, projectId: int, vuln_id: int, link_id: str) -> bool:
        """Unlink an issue from vulnerability

        Args:
            serverUrl (str): Server URL (https://gitlab.com)
            projectId (int): Project that issue exists in
            vuln_id (int): Vulnerability report ID
            issueWebUrl (str): Issue Web URL

        Returns:
            bool: True issue unlinked
        """

        url = self._urljoin(serverUrl, 'api/v4/vulnerabilities', vuln_id, 'issue_links', link_id)
        res = requests.delete(url, headers=self._default_headers())
        res.raise_for_status()

        return True

    def _check_issue_labels(
            self,
            issue: Dict[str, Any],
            exclude_labels: Optional[List[str]] = None,
            include_labels: Optional[List[str]] = None,
            prefixes: bool = False) -> bool:
        """Check issue labels against include and exclude labels

        Args:
            issue (Dict[str, Any]): Issue dictionary
            exclude_labels (Optional[List[str]], optional): List of labels to exlucde. Defaults to None.
            include_labels (Optional[List[str]], optional): List of labels to include. Defaults to None.
            prefixes (bool, optional): Treat exclude/include labels as prefixes, matching with startswith(). Defaults to False.

        Raises:
            Exception: Error, Issue does not have labels key.

        Returns:
            bool: True if labels pass, False if labels fail check
        """
        if 'labels' not in issue:
            raise Exception('Eror, Issue does not have labels key.')

        labels = issue['labels']
        found_include_label = False

        for label in labels:
            if exclude_labels is not None:
                for exclude_label in exclude_labels:
                    if prefixes:
                        if label.startswith(exclude_label):
                            return False
                    else:
                        if label == exclude_label:
                            return False

            if include_labels is not None:
                for include_label in include_labels:
                    if prefixes:
                        if label.startswith(include_label):
                            found_include_label = True
                    else:
                        if label == include_label:
                            found_include_label = True

        if include_labels is not None and not found_include_label:
            return False

        return True

    def _calculate_labels(
            self,
            existing_labels: List[str],
            add_labels: Optional[List[str]],
            remove_labels: Optional[List[str]]) -> List[str]:
        """Calulate labels to set on an issue.

        This method tries to simulate the behavior of GitLab when setting labels on an issue.

        Args:
            existing_labels (List[str]): Existing labels to update
            add_labels (List[str]): Labels being added
            remove_labels (List[str]): Labels being removed

        Returns:
            List[str]: List of labels to set on the issue
        """

        labels = []
        labels.extend(existing_labels)

        if remove_labels is not None:
            for remove_label in remove_labels:
                if remove_label in labels:
                    labels.remove(remove_label)

        if add_labels is not None:
            for add_label in add_labels:
                if add_label in labels:
                    continue

                related_label = self._labels_find_related(labels, add_label)
                if related_label is not None:
                    labels.remove(related_label)

                labels.append(add_label)

        return labels

    def _labels_find_related(self, labels: List[str], label: str) -> Optional[str]:
        """Find related labels. (ex. 'A::B' and 'A::C' are related)

        Args:
            labels (List[str]): All existing labels
            label (str): Label to check for a related label

        Returns:
            Optional[str]: Related label or None
        """

        if label.find('::') == -1:
            return None

        prefix = label[:label.rfind('::') + 2]

        for item in labels:
            if item.startswith(prefix):
                # Make sure we don't match a prefix of a prefix
                # ex. 'foo::bar::bat' (prefix 'foo::bar::')
                #   and 'foo::bar::bat::baz' (prefix 'foo::bar::bat::')
                p = item[:item.rfind('::') + 2]
                if p == prefix:
                    return item

        return None

    def _issue_close(
            self,
            issueWebUrl: str,
            exclude_labels: Optional[List[str]] = None,
            include_labels: Optional[List[str]] = None,
            add_labels: Optional[List[str]] = None,
            remove_labels: Optional[List[str]] = None,
            prefixes: bool = False) -> bool:
        """Close an open issue

        Args:
            issueWebUrl (str): Issue Web URL
            exclude_labels (Optional[List[str]], optional): List of labels to exlucde. Defaults to None.
            include_labels (Optional[List[str]], optional): List of labels to include. Defaults to None.
            add_labels (Optional[List[str]], optional): List of labels to set. Defaults to None.
            prefixes (bool, optional): Treat exclude/include labels as prefixes, matching with startswith(). Defaults to False.

        Raises:
            Exception: Error, Issue does not have state key.

        Returns:
            bool: True if state was changed, False if issue was already closed
        """

        if issueWebUrl.find(self.ci_project_path) > -1:
            issue_project_id = int(self.ci_project_id)
        elif issueWebUrl.find('gitlab-org/gitlab'):
            issue_project_id = 278964  # gitlab-org/gitlab project id
        else:
            self._logger.warning('_issue_close: Unknown project path, not closing: ' + issueWebUrl)
            return False

        issueRestUrl = self._issue_rest_url_from_weburl(self.ci_server_url, issue_project_id, issueWebUrl)
        self._logger.debug('_issue_close: issueRestUrl:' + issueRestUrl)

        res = requests.get(f"{issueRestUrl}", headers=self._default_headers())
        res.raise_for_status()
        data = res.json()

        if 'state' not in data:
            self._logger.error('Error, Issue does not have state key.')
            self._logger.debug('_issue_close: data: ' + str(data))
            raise Exception('Error, Issue does not have state key.')

        # Check for state closed
        state = data['state']
        if state == 'closed':
            return False

        # Check our labels
        if not self._check_issue_labels(data, exclude_labels=exclude_labels, include_labels=include_labels, prefixes=prefixes):
            self._logger.debug("_issue_close: Labels don't match, not closing")
            return False

        if not self.dry_run:
            if add_labels is not None or remove_labels is not None:

                labels = self._calculate_labels(data['labels'], add_labels, remove_labels)
                labels_url = urllib.parse.quote(','.join(labels))

                self._logger.debug(f"_issue_close: Setting labels: {','.join(labels)}")

                res = requests.put(f"{issueRestUrl}?state_event=close&labels={labels_url}", headers=self._default_headers())
                res.raise_for_status()

            else:
                res = requests.put(f"{issueRestUrl}?state_event=close", headers=self._default_headers())
                res.raise_for_status()

        return True

    def fix_issue_labels(self) -> int:
        # 1. Get a list of all issues in project
        # 2. Check that labels are correct

        bad_labels = [
            'group::composition analysis',
            'Category:Container Scanning',
        ]

        issue_url = self._urljoin(
            self.ci_server_url, 'api/v4/projects', self.ci_project_id, 'issues')

        # Get all of the issues using pagination

        self._logger.info('Fix labels for existing issues...')

        issues = []
        next_page = 1
        total_pages = 10
        total_issues = -1
        current_page = -1

        while current_page < total_pages:

            url = issue_url + '?per_page=100&page=' + str(next_page)
            self._logger.debug('fix_issue_labels: url: ' + url)
            res = requests.get(url, headers=self._default_headers())
            res.raise_for_status()

            total_pages = int(res.headers['x-total-pages'])
            total_issues = int(res.headers['x-total'])
            current_page = int(res.headers['x-page'])

            if current_page != next_page:
                self._logger.debug('fix_issue_labels: current_page: ' + str(current_page))
                raise Exception('fix_issue_labels: current_page != next_page')

            if next_page != total_pages:
                next_page = int(res.headers['x-next-page'])

            self._logger.debug(f"fix_issue_labels: next_page: {next_page}, total_pages: {total_pages}, total_items: {total_issues}")

            data = res.json()

            if type(data) is list and len(data) < 1:
                break

            self._logger.debug('fix_issue_labels: len(data): ' + str(len(data)))

            issues.extend(data)

        if len(issues) != total_issues:
            raise Exception('fix_issue_labels: len(issues) != total_issues')

        # self._logger.debug('fix_labels: data: ' + str(data))

        # Process each issue
        self._logger.info(f"  - Processing {len(issues)} issues...")
        for issue in issues:
            self._logger.info(f"  - Issue: {issue['title']}")

            labels = issue['labels']

            new_labels = ['group::dynamic analysis', 'Category:API Security']
            has_bad_label = False

            for label in labels:
                if label in bad_labels:
                    has_bad_label = True
                    continue

                new_labels.append(label)

            if has_bad_label:
                self._logger.info('  - Fixing bad labels')
                url = f"{issue_url}/{issue['iid']}?labels=" + (','.join(new_labels)).replace(' ', '+')
                # self._logger.debug('fix_issue_labels: url: ' + url)
                res = requests.put(url, headers=self._default_headers())
                res.raise_for_status()

        return len(issues)

    def unlink_issues_from_detected(self) -> None:

        self._logger.info('Unlink issues for re-detected vulnerabilities...')

        if not self.filter_image:
            raise Exception("Error, self.filter_image is not set")

        self._logger.info(f"  Getting vulnerabilities for '{self.ci_project_path}'...")
        resolvedVulns = self._get_vulnerabilities(
            self.ci_project_path,
            states=[
                # VulnerabilityState.CONFIRMED,
                VulnerabilityState.DETECTED,
                # VulnerabilityState.DISMISSED,
                # VulnerabilityState.RESOLVED
            ],
            reportTypes=[VulnerabilityReportType.CONTAINER_SCANNING],
            severities=[
                VulnerabilitySeverity.CRITICAL, VulnerabilitySeverity.HIGH,
                VulnerabilitySeverity.MEDIUM,
                VulnerabilitySeverity.LOW, VulnerabilitySeverity.INFO,
                VulnerabilitySeverity.UNKNOWN
            ],
            filter_image=self.filter_image
        )

        projectIssueUrlPrefix = self._urljoin(self.ci_server_url, self.ci_project_path)

        self._logger.info(f"  Processing {len(resolvedVulns)} vulnerabilities...")
        for vuln in resolvedVulns:

            self._logger.info(f"  Proccessing '{vuln['title']}' ({str(vuln['id'])})...")

            if len(vuln['issueLinks']['nodes']) > 0:
                self._logger.info('    - Unlinking linked issues...')

            for issue in vuln['issueLinks']['nodes']:
                link_gid = issue['issue']['id']
                issueUrl = issue['issue']['webUrl']
                link_id = link_gid[link_gid.rindex('/') + 1:]
                vuln_id = self._vulnerability_id_from_gid(vuln['id'])

                # Skip issues not in our project
                if not issueUrl.startswith(projectIssueUrlPrefix):
                    continue

                self._logger.info(f"    - Unlinking: {issueUrl}")
                if not self.dry_run:
                    self._issue_unlink(self.ci_server_url, int(self.ci_project_id), vuln_id, link_id)

        self._logger.info("Done")

    def dismissed_to_resolved(self) -> None:

        # 1. Query all vulnerabilities with status of dismissed
        # 2. Change status of each to resolved
        self._logger.info('Change dismissed vulnerabilities to resolved on default branch...')

        if not self.filter_image:
            raise Exception("Error, self.filter_image is not set")

        self._logger.info(f"  Getting vulnerabilities for {self.ci_project_path}...")
        resolvedVulns = self._get_vulnerabilities(
            self.ci_project_path,
            states=[
                VulnerabilityState.DISMISSED,
            ],
            reportTypes=[VulnerabilityReportType.CONTAINER_SCANNING],
            severities=[
                VulnerabilitySeverity.CRITICAL, VulnerabilitySeverity.HIGH,
                VulnerabilitySeverity.MEDIUM,
                VulnerabilitySeverity.LOW, VulnerabilitySeverity.INFO,
                VulnerabilitySeverity.UNKNOWN
            ],
            filter_image=self.filter_image,
        )

        self._logger.info(f"  Processing {len(resolvedVulns)} vulnerabilities...")
        for vuln in resolvedVulns:

            self._logger.info(f"  Proccessing '{vuln['title']}' ({str(vuln['id'])})...")
            self._logger.info('    - Setting state to RESOLVED')
            if not self.dry_run:
                self._vulnerability_update_state(self.ci_server_url, vuln['id'], VulnerabilityState.RESOLVED)

        self._logger.info("Done")

    def resolve_nolongerfound_vulnerabilities(
            self,
            close_linked_issues: bool = True,
            close_dr_issues: bool = False) -> int:
        """Resolve vulnerabilities that are no longer found on default branch

        Args:
            close_linked_issues (bool, optional): Close linked issues that exist in current project. Defaults to True.
            close_dr_issues (bool, optional): Close linked DR issues that exist in current project. Defaults to False.

        Returns:
            int: Count of vulnerabiltiies marked as DISMISSED
        """

        # 1. Get list of all vulnerabilities with
        #    resolvedOnDefaultBranch = true and state != DISMISSED
        # 2. If the vulnerability is linked to an issue, close the issue
        # 3. Change vulnerability state to DISMISSED

        self._logger.info('Resolving vulnerabilities no longer found on default branch...')

        if not self.filter_image:
            raise Exception("Error, self.filter_image is not set")

        self._logger.info('  Getting vulnerabilities...')
        self._logger.debug(f"resolve_nolongerfound_vulnerabilities: ci_project_path: {self.ci_project_path}")
        resolvedVulns = self._get_vulnerabilities(
            self.ci_project_path,
            states=[
                VulnerabilityState.CONFIRMED,
                VulnerabilityState.DETECTED,
                # VulnerabilityState.DISMISSED,
                # VulnerabilityState.RESOLVED
            ],
            reportTypes=[VulnerabilityReportType.CONTAINER_SCANNING],
            severities=[
                VulnerabilitySeverity.CRITICAL,
                VulnerabilitySeverity.HIGH,
                VulnerabilitySeverity.MEDIUM,
                VulnerabilitySeverity.LOW,
                VulnerabilitySeverity.INFO,
                VulnerabilitySeverity.UNKNOWN
            ],
            filter_image=self.filter_image,
            resolvedOnDefaultBranch=True
        )

        self._logger.info(f"  Processing {len(resolvedVulns)} vulnerabilities...")
        for vuln in resolvedVulns:

            self._logger.info(f"  Proccessing {vuln['id']}:'{vuln['title']}'...")

            if close_linked_issues:
                self._logger.info('    - Closing linked issues')

                for issue in vuln['issueLinks']['nodes']:
                    issueUrl = issue['issue']['webUrl']

                    self._logger.info(f"    - Closing: {issueUrl}")

                    exclude_labels = None
                    if close_dr_issues:
                        exclude_labels = [self._fedramp_dr_label_prefix]

                    if self._issue_close(issueUrl, exclude_labels=exclude_labels, prefixes=True):
                        self._logger.info('    - Issue closed')
                    else:
                        self._logger.info('    - Issue not closed due to label exclusion')

            self._logger.info('    - Setting state to RESOLVED')
            if not self.dry_run:
                self._vulnerability_update_state(self.ci_server_url, vuln['id'], VulnerabilityState.RESOLVED)

        self._logger.info("Done")
        return len(resolvedVulns)

    def close_issues(
            self,
            vulns_resolved: bool,
            vulns_nolongerfound: bool,
            close_dr_issues: bool,
            exclude_labels: Optional[str],
            inclue_labels: Optional[str],
            labels_are_prefixes: bool,
            add_labels: Optional[str],
            remove_labels: Optional[str]) -> int:

        self._logger.info('Closing linked issues...')

        if not self.filter_image:
            raise Exception("Error, self.filter_image is not set")

        if not vulns_resolved and not vulns_nolongerfound:
            msg = "Error, no vulnerabilities selected to close issues for. Please provide one or both of --vulns-resolved or --vulns-nolongerfound."
            self._logger.error(msg)
            raise Exception(msg)

        states = []
        resolvedOnDefaultBranch = None

        if vulns_resolved:
            states.append(VulnerabilityState.RESOLVED)

        if vulns_nolongerfound:
            states.append(VulnerabilityState.CONFIRMED)
            states.append(VulnerabilityState.DETECTED)
            resolvedOnDefaultBranch = True

        self._logger.info(f'  Getting vulnerabilities for {self.ci_project_path}...')
        vulns = self._get_vulnerabilities(
            self.ci_project_path,
            states=states,
            reportTypes=[VulnerabilityReportType.CONTAINER_SCANNING],
            severities=[
                VulnerabilitySeverity.CRITICAL,
                VulnerabilitySeverity.HIGH,
                VulnerabilitySeverity.MEDIUM,
                VulnerabilitySeverity.LOW,
                VulnerabilitySeverity.INFO,
                VulnerabilitySeverity.UNKNOWN
            ],
            filter_image=self.filter_image,
            resolvedOnDefaultBranch=resolvedOnDefaultBranch
        )

        if add_labels is not None:
            add_labels_list = add_labels.split(',')
        else:
            add_labels_list = None

        if remove_labels is not None:
            remove_labels_list = remove_labels.split(',')
        else:
            remove_labels_list = None

        self._logger.info(f"  Processing {len(vulns)} vulnerabilities...")
        for vuln in vulns:

            self._logger.info(f"  Proccessing '{vuln['title']}'...")
            self._logger.info('    - Closing linked issues')

            for issue in vuln['issueLinks']['nodes']:
                issueUrl = issue['issue']['webUrl']

                self._logger.info(f"    - Closing: {issueUrl}")

                arg_exclude_labels = None
                arg_include_labels = None

                if exclude_labels is not None:
                    arg_exclude_labels = exclude_labels.split(',')
                if inclue_labels is not None:
                    arg_include_labels = inclue_labels.split(',')

                if not close_dr_issues:
                    if arg_exclude_labels is None:
                        arg_exclude_labels = []

                    arg_exclude_labels.append(self._fedramp_dr_label_prefix)
                    labels_are_prefixes = True
                    self._logger.debug('    - Setting labels_are_prefixes to True due to close_dr_issues=True')

                if self._issue_close(
                        issueUrl,
                        exclude_labels=arg_exclude_labels,
                        include_labels=arg_include_labels,
                        prefixes=labels_are_prefixes,
                        add_labels=add_labels_list,
                        remove_labels=remove_labels_list):

                    self._logger.info('    - Issue closed')
                else:
                    self._logger.info('    - Issue not closed due to label exclusion')

        self._logger.info("Done")
        return len(vulns)

    def _create_vendor_dependency_issue(
        self,
        server_url: str, project_id: int,
        issue_title: str,
        vuln_title: str, vuln_url: str,
        image: str, image_sha256: str,
        severity: str,
        detected_date: str
    ) -> str:
        """Create a vendor dependency DR issue

        Args:
            server_url (str): _description_
            project_id (int): _description_
            issue_title (str): _description_
            vuln_title (str): _description_
            vuln_url (str): _description_
            image (str): _description_
            image_sha256 (str): _description_
            severity (str): _description_
            detected_date (str): _description_

        Raises:
            IssueCreationException: _description_

        Returns:
            str: Web URL of the created issue
        """

        template = self.jinja_env.get_template('dr_vendor_dependency.md.jinja')
        issue_body = template.render(
            vuln_title=vuln_title,
            vuln_url=vuln_url,
            image=image,
            image_sha256=image_sha256,
            severity=severity,
            detected_date=detected_date
        )

        issue_json = {
            'title': issue_title,
            'description': issue_body,
            'labels': ','.join([
                "FedRAMP Deviation Request",
                "FedRAMP Vendor Dependency",
                "FedRAMP DR Status::Ready for Review",
            ]),
            'confidential': True,
        }

        url = self._urljoin(server_url, 'projects', project_id, 'issues')
        res = requests.post(url, json=issue_json, headers=self._default_headers())
        res.raise_for_status()
        data = res.json()

        if 'webUrl' not in data:
            raise IssueCreationException(str(data))

        return data['webUrl']

# end
