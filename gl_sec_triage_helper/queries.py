
from enum import Enum


class VulnerabilityState(str, Enum):
    DETECTED = 'DETECTED'
    CONFIRMED = 'CONFIRMED'
    RESOLVED = 'RESOLVED'
    DISMISSED = 'DISMISSED'


class VulnerabilitySeverity(str, Enum):
    INFO = 'INFO'
    UNKNOWN = 'UNKNOWN'
    LOW = 'LOW'
    MEDIUM = 'MEDIUM'
    HIGH = 'HIGH'
    CRITICAL = 'CRITICAL'


class VulnerabilityReportType(str, Enum):
    SAST = 'SAST'
    DEPENDENCY_SCANNING = 'DEPENDENCY_SCANNING'
    CONTAINER_SCANNING = 'CONTAINER_SCANNING'
    DAST = 'DAST'
    SECRET_DETECTION = 'SECRET_DETECTION'
    COVERAGE_FUZZING = 'COVERAGE_FUZZING'
    API_FUZZING = 'API_FUZZING'
    CLUSTER_IMAGE_SCANNING = 'CLUSTER_IMAGE_SCANNING'
    GENERIC = 'GENERIC'


class Queries(object):
    '''Collection of graphql queries
    '''

    ProjectVulnerabilityQuery = """
query ($fullPath: ID!, $after: String, $first: Int, $severity: [VulnerabilitySeverity!], $reportType: [VulnerabilityReportType!], $state: [VulnerabilityState!]) {
  project(fullPath: $fullPath) {
    vulnerabilities(
      after: $after
      first: $first
      severity: $severity
      reportType: $reportType
      state: $state
    ) {
      nodes {
        ... on Vulnerability {
          id
          title
          vulnerabilityPath
          descriptionHtml
          severity
          project {
            nameWithNamespace
            webUrl
          }
          scanner {
            name
          }
          location {
            ... on VulnerabilityLocationContainerScanning {
              operatingSystem
              image
            }
          }
          primaryIdentifier {
            url
            externalId
          }
          issueLinks {
            nodes {
              issue {
                id,
                webUrl
              }
            }
          }
          state
          reportType
          resolvedOnDefaultBranch
        }
      }
      pageInfo {
        ... on PageInfo {
          hasNextPage
          endCursor
        }
      }
    }
  }
}
    """

    ConfirmVulnerabilityMutation = """
        mutation(
        $vulnerabilityId: VulnerabilityID!
        ) {
        vulnerabilityConfirm(input: {
            id: $vulnerabilityId
        })
        {
            errors
            vulnerability {
            id
            state
            }
        }
        }
    """

    GetVulnerabilityQuery = """
        query ($vulnerabilityId: VulnerabilityID!) {
        vulnerability(id: $vulnerabilityId) {
            id
            title
            state
        }
        }
    """
