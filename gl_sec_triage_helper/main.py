
import click
import logging
import logging.config
from .app import Application
from typing import Any, List, Dict, Optional  # noqa: F401

logger: logging.Logger


def configure_logging() -> None:
    log_file = 'gl-sec-triage-helper.log'

    logging_config = {
        "version": 1,
        'disable_existing_loggers': True,
        "loggers": {
            "": {
                "handlers": [
                    "console",
                    "file",
                ],
                "level": "DEBUG",
                "propagate": False,
            },
            "Application": {
                "handlers": [
                    "console",
                    "file",
                ],
                "level": "DEBUG",
                "propagate": False,
            },
        },
        "formatters": {
            "console": {
                "format": "%(asctime)s [%(levelname)-3.3s] : %(message)s",
                "datefmt": "%H:%M:%S",
            },
            "file": {
                "format": "%(asctime)s [%(levelname)-3.3s] : %(message)s",
                "datefmt": "%Y-%m-%d %H:%M:%S",
            },
        },
        "handlers": {
            "console": {
                "class": "logging.StreamHandler",
                "level": "DEBUG",
                "stream": "ext://sys.stdout",
                "formatter": "console",
            },
        }
    }

    if log_file:
        logging_config['handlers']['file'] = {
            "class": "logging.FileHandler",
            "filename": log_file,
            "level": "DEBUG",
            "formatter": "file",
        }
    else:
        logging_config['handlers']['file'] = {
            "class": "logging.NullHandler",
        }

    logging.config.dictConfig(logging_config)

    global logger
    logger = logging.getLogger()


def run() -> None:
    configure_logging()
    cli(obj={})


def _set_obj_on_app(ctx: click.Context, app: Application) -> None:
    """Set all the ctx.obj key/values on the app instance.

    Keys should be limited to parameters that have been defined.

    Args:
        ctx (click.Context): Click context object
        app (Application): Application instance
    """
    for key, value in ctx.obj.items():
        setattr(app, key, value)


@click.group()
@click.option('--ci_server_url',   envvar='CI_SERVER_URL',     required=True)  # noqa: E241
@click.option('--ci_project_id',   envvar='CI_PROJECT_ID',     required=True)  # noqa: E241
@click.option('--ci_project_path', envvar='CI_PROJECT_PATH',   required=True)  # noqa: E241
@click.option('--filter-image',    envvar='GSTH_FILTER_IMAGE', default='-fips',  # noqa: E241
    help="Container image to filter vulnerabilities. Defaults to '-fips'")  # noqa: E128
@click.option('--token',           envvar='GSTH_TOKEN',        required=True,  # noqa: E241
    help='GitLab authentication token')  # noqa: E128
@click.option('--verbose',         envvar='GSTH_VERBOSE',      is_flag=True, default=False,  # noqa: E241
    help='Enable verbose (debug) output')  # noqa: E128
@click.option('--dry-run',         envvar='GSTH_DRY_RUN',      is_flag=True, default=False,  # noqa: E241
    help='Dry run. Do not make any changes.')  # noqa: E128
@click.pass_context
def cli(ctx: click.Context,
        ci_server_url: str, ci_project_id: str, ci_project_path: str,
        filter_image: str, token: str, verbose: bool, dry_run: bool
        ) -> None:

    logger.debug(f"option: ci_server_url: {ci_server_url}")
    logger.debug(f"option: ci_project_id: {ci_project_id}")
    logger.debug(f"option: ci_project_path: {ci_project_path}")
    logger.debug(f"option: filter-image: {filter_image}")
    logger.debug(f"option: token: {token}")
    logger.debug(f"option: verbose: {verbose}")
    logger.debug(f"option: dry-run: {dry_run}")

    ctx.obj['ci_server_url'] = ci_server_url
    ctx.obj['ci_project_id'] = ci_project_id
    ctx.obj['ci_project_path'] = ci_project_path
    ctx.obj['filter_image'] = filter_image
    ctx.obj['token'] = token
    ctx.obj['verbose'] = verbose
    ctx.obj['dry_run'] = dry_run


@cli.command(help='Resolve no longer found container scanning vulnerabilities and (by default) close linked issues.')
@click.option('--close_linked_issues/--no-close_linked_issues', default=True,
    help='Close linked issues True or False. Defaults to True.')  # noqa: E128
@click.option('--close-dr-issues/--no-close-dr-issues', default=False,
    help='Close linked DR issues. Defaults to False.')  # noqa: E128
@click.pass_context
def resolve_nolongerfound(ctx: click.Context, close_linked_issues: bool, close_dr_issues: bool) -> None:
    logger.debug("option: close_linked_issues: " + str(close_linked_issues))
    logger.debug("option: close_dr_issues: " + str(close_dr_issues))

    app = Application()
    _set_obj_on_app(ctx, app)
    app.initialize()
    app.resolve_nolongerfound_vulnerabilities(
        close_linked_issues=close_linked_issues,
        close_dr_issues=close_dr_issues)


@cli.command(help='Close linked issues')
@click.option(
    '--vulns-resolved', is_flag=True, default=False,
    help='Process issues linked to resolved vulnerabilities.')
@click.option(
    '--vulns-nolongerfound', is_flag=True, default=False,
    help='Process issues linked to no-longer-found vulnerabilities.')
@click.option(
    '--close-dr-issues', is_flag=True, default=False,
    help='Close linked DR issues.')
@click.option(
    '--exclude-labels', default=None,
    help='Exclude issues based on these labels. Comma separated list of labels.')
@click.option(
    '--include-labels', default=None,
    help='Include only issues with these labels. Comma separated list of labels.')
@click.option(
    '--labels-are-prefixes', is_flag=True, default=False,
    help='Match supplied labels using startswith(). Defaults to False.')
@click.option(
    '--add-labels', default=None,
    help='Add these labels on closed issues. Comma separated list of labels.')
@click.option(
    '--remove-labels', default=None,
    help='Remove these labels from closed issues. Comma separated list of labels.')
@click.pass_context
def close_issues(
        ctx: click.Context,
        vulns_resolved: bool,
        vulns_nolongerfound: bool,
        close_dr_issues: bool,
        exclude_labels: Optional[str],
        include_labels: Optional[str],
        labels_are_prefixes: bool,
        add_labels: Optional[str],
        remove_labels: Optional[str]) -> None:

    logger.debug("option:     close_dr_issues: " + str(close_dr_issues))
    logger.debug("option:      exclude_labels: " + str(exclude_labels))
    logger.debug("option:      include_labels: " + str(include_labels))
    logger.debug("option: labels_are_prefixes: " + str(labels_are_prefixes))
    logger.debug("option:       remove_labels: " + str(remove_labels))
    logger.debug("option:          add_labels: " + str(add_labels))
    logger.debug("option:      vulns_resolved: " + str(vulns_resolved))
    logger.debug("option: vulns_nolongerfound: " + str(vulns_nolongerfound))

    app = Application()
    _set_obj_on_app(ctx, app)
    app.initialize()
    app.close_issues(
        vulns_resolved, vulns_nolongerfound, close_dr_issues,
        exclude_labels, include_labels, labels_are_prefixes,
        add_labels, remove_labels)


@cli.command(help='Change all dismissed container scanning vulnerabilities to resolved.')
@click.pass_context
def dismissed_to_resolved(ctx: click.Context) -> None:
    app = Application()
    _set_obj_on_app(ctx, app)
    app.initialize()
    app.dismissed_to_resolved()


if __name__ == '__main__':
    run()

# end
