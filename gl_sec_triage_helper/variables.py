
import logging
import os
from typing import Any, List, Optional


class Variable(object):
    name: str
    default: Any
    attribute: str
    aliases: List[str]
    use_prefix: bool
    type: Any
    required: bool

    def __init__(
            self,
            name: str,
            default: Any = None,
            attribute: Optional[str] = None,
            aliases: Optional[List[str]] = None,
            use_prefix: bool = True,
            type: Any = None,
            required: bool = False) -> None:
        """Define an expected environment variable.

        Args:
            name (str): Name of method variable to create, also default name for environment variable (minus a prefix)
            default (Any, optional): [optional] Default value. Defaults to None.
            attribute (Optional[str], optional): [optional] Use a different name for the class variable to set from this definition. Defaults to None.
            aliases (Optional[List[str]], optional): [optional] List of alternate environment variable names. Defaults to None.
            use_prefix (bool, optional): [optiona] Use prefixed defined in VariableHandler. Defaults to True.
            type (Any, optional): [optiona] Type to cast value to. Defaults to None.
            required (bool, optional): Is variable requried. If true, errors if not set.
        """
        self.name = name
        self.default = default
        self.use_prefix = use_prefix
        self.type = type
        self.required = required

        if attribute is None:
            self.attribute = name
        else:
            self.attribute = attribute

        if aliases is None:
            self.aliases = []
        else:
            self.aliases = aliases


class VariableHandler(object):
    app: Any
    logger: logging.Logger
    prefix: str
    variables: List[Variable]

    def __init__(self, prefix: str, app: Any, variables: List[Variable], logger: logging.Logger) -> None:
        """Process defined Variables and add them to app.

        Args:
            prefix (str): Prefix for environment variables
            app (Any): Application object to set variables on
            variables (List[Variable]): List of Variable definitions to look for
            logger (logging.Logger): Logger instance to log variables being set to debug log
        """

        self.app = app
        self.variables = variables
        self.logger = logger
        self.prefix = prefix

    def __process_variable(self, var: Variable) -> None:

        prefix = ""
        if var.use_prefix:
            prefix = self.prefix + "_"

        name = f"{prefix}{var.name.upper()}"
        value = os.environ.get(name)

        if value is None:
            for alias in var.aliases:
                alias = f"{prefix}{alias.upper()}"
                value = os.environ.get(alias)
                if value is not None:
                    break

        if callable(var.type) and value is not None:
            value = var.type(value)

        if value is None and var.required:
            self.logger.error(f"Error, required variable '{name}' was not found.")
            exit(-1)

        if value is None:
            value = var.default

        self.logger.debug(f"Setting '{var.name}' with value of '{value}' on app")
        setattr(self.app, var.attribute, value)

    def process_variables(self) -> None:

        self.app.env_prefix = self.prefix

        for var in self.variables:
            self.__process_variable(var)
