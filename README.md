# gitlab-sec-triage-helper

This is a helper script to go along with the `security-triage-automation` script.

## Usage

The following commands are available:

| Command | Description |
|---------|-------------|
|[close-issues](#close-issues-linked-to-vulnerabilities)|Close issues linked to vulnerabilities|
|[dismissed-to-resolved](#change-dismissed-vulnerabilities-to-resolved)|Change vulnerability status from dismissed to resolved|
|[resolve-nolongerfound](#resolve-no-longer-found-vulnerabilities-and-optionally-close-linked-issues)|Change the status of vulnerabilities no longer found to resolved|

### Resolve no longer found vulnerabilities and, optionally, close linked issues

Your vendor finally fixed some CVEs and now you need to close some vulnerabilities and (optional, but by default) close the linked issue located in the `gitlab-org/gitlab` project.

This command will do the following:

- Find all container scanning vulnerabilities and filter them:
  - Filter by Container Scanning
  - Filter by `filter-image` argument
  - Filter by `noLongerFound` is True
- Resolve the vulnerability
- By default, close linked issues excluding those with a `FedRamp::DR Status::` prefixed label
  - To close the linked DR issues, provide the `--close-dr-issues` argument.

#### Options

The following are options for the `resolve-nolongerfound` command. Some options are global and must come BEFORE the command. Other are specific for the command and are provided AFTER the command name. The _Global_ column in the below table indicates which are which.

| Option | Required | Default | Global | Description |
|--------|----------|---------|--------|-------------|
|ci_server_url         | Y | | Y | The server URL (e.g. https://gitlab.com) |
|ci_project_id         | Y | | Y | The project ID with vulnerabilities      |
|ci_project_path       | Y | | Y | The project path with vulnerabilities (must match project ID)  |
|dry-run               | N | N | Y | Show what would happen, but don't make any actual changes |
|filter_image          | N | `-fips` | Y | Filter vulnerabilities based on image |
|no-close_linked_issues| N | | N | Don't close linked issues |
|close-dr-issues       | N | False | N | Close issues with `FedRamp::DR Status::` label prefix. Defaults to not closing dr issues |

#### Running

To run you will need the ability to run Docker containers. For OS X (Mac), a common method is to install [Rancher Desktop](https://rancherdesktop.io/), a free alternative to Docker Desktop.

1. Pull the image from the GitLab registry:

   ```shell
   docker pull registry.gitlab.com/mikeeddington/gitlab-sec-triage-helper:1
   docker tag registry.gitlab.com/mikeeddington/gitlab-sec-triage-helper:1 gl-sec-triage-helper
   ```

1. Run the command. You might consider creating a short shell script to make editing the arguments easier. **Note** To prevent the closing of linked issues add the `--no-close_linked_issues` argument.

   ```shell
   docker run --rm gl-sec-triage-helper \
       --ci_server_url https://gitlab.com \
       --ci_project_id 12345 \
       --ci_project_path mikeeddington/gitlab-sec-triage-helper \
       --filter-image registry.gitlab.com/mikeeddington/gitlab-sec-triage-helper:1-fips \
       --token XXXXXX \
      resolve-nolongerfound
   ```

   _Don't closeout linked issues:_

   ```shell
   docker run --rm gl-sec-triage-helper \
       --ci_server_url https://gitlab.com \
       --ci_project_id 12345 \
       --ci_project_path mikeeddington/gitlab-sec-triage-helper \
       --filter-image registry.gitlab.com/mikeeddington/gitlab-sec-triage-helper:1-fips \
       --token XXXXXX \
      resolve-nolongerfound --no-close_linked_issues
   ```

   _Closeout linked issues, including DR issues:_

   ```shell
   docker run --rm gl-sec-triage-helper \
       --ci_server_url https://gitlab.com \
       --ci_project_id 12345 \
       --ci_project_path mikeeddington/gitlab-sec-triage-helper \
       --filter-image registry.gitlab.com/mikeeddington/gitlab-sec-triage-helper:1-fips \
       --token XXXXXX \
      resolve-nolongerfound --close-dr-issues
   ```

### Close issues linked to vulnerabilities

This command automates the closing of issues linked to vulnerabilities.

This command will do the following:

- Find all container scanning vulnerabilities and filter them:
  - Filter by Container Scanning
  - Filter by `filter-image` argument
  - Filter by state based on arguments:
    - `--vulns-resolved`
      - State == RESOLVED
    - `--vulns-nolongerfound`
      - Filter by `noLongerFound` is True
- Find all linked issues and filter them based on:
  - `--exclude-labels` -- Issues with a matching label will be excluded.
  - `--include-labels` -- Issues with a matching label will be included.
  - `--labels-are-prefixes` -- Match labels with startswith()
  - `--close-dr-issues` -- By default issues with a `FedRamp::DR Status::` prefix are excluded. This option includes them.
- Close linked issues after filtering them

#### Options

The following are options for the `close-issues` command. Some options are global and must come BEFORE the command. Other are specific for the command and are provided AFTER the command name. The _Global_ column in the below table indicates which are which.

| Option | Required | Default | Global | Description |
|--------|----------|---------|-----------|-------------|
|ci_server_url         | Y | | Y | The server URL (e.g. https://gitlab.com) |
|ci_project_id         | Y | | Y | The project ID with vulnerabilities      |
|ci_project_path       | Y | | Y | The project path with vulnerabilities (must match project ID)  |
|dry-run               | N | N | Y | Show what would happen, but don't make any actual changes |
|filter_image          | N | `-fips` | Y | Filter vulnerabilities based on image |
|vulns-resolved        |   | | N | Select vulnerabilities with a state of RESOLVED |
|vulns-nolongerfound   |   | | N | Select vulnerabilities that are no longer found |
|close-dr-issues       | N | False | N | Close issues with `FedRamp::DR Status::` label prefix. Defaults to not closing dr issues |
|exclude-labels        | N | | N | Filter out issues that have one or more of the provided labels. Comma separated list of values. |
|include-labels        | N | | N | Include issues that have one or more of the provided labels. Comma separated list of values. |
|add-labels            | N | | N | Add the provided labels when closing the issue. Existing labels are kept with the exception of labels that are related (`workflow::in dev` and `workflow::completed` are related. Adding `workflow::completed` removes `workflow::in dev`). Comma separated list of values. |
|remove-labels         | N | | N | Remove the provided labels when closing the issue. Existing labels are kept with the exception of related labels. Comma separated list of values. |

#### Running

To run you will need the ability to run Docker containers. For OS X (Mac), a common method is to install [Rancher Desktop](https://rancherdesktop.io/), a free alternative to Docker Desktop.

1. Pull the image from the GitLab registry:

   ```shell
   docker pull registry.gitlab.com/mikeeddington/gitlab-sec-triage-helper:1
   docker tag registry.gitlab.com/mikeeddington/gitlab-sec-triage-helper:1 gl-sec-triage-helper
   ```

1. Run the command. You might consider creating a short shell script to make editing the arguments easier.

   ```shell
   docker run --rm gl-sec-triage-helper \
       --ci_server_url https://gitlab.com \
       --ci_project_id 12345 \
       --ci_project_path mikeeddington/gitlab-sec-triage-helper \
       --filter-image registry.gitlab.com/mikeeddington/gitlab-sec-triage-helper:1-fips \
       --token XXXXXX \
      close-issues --vulns-resolved
   ```

### Change dismissed vulnerabilities to resolved

Did you change the vulnerability state to DISMISSED, but realized later you wanted them to be RESOLVED, so the vulnerability would re-open if found again? Then this is the command for you!

This command will do the following:

- Find all container scanning vulnerabilities and filter them with `filter-image` argument
- Update the state to RESOLVED if it's set to DISMISSED

To run you will need the ability to run Docker containers. For OS X (Mac), a common method is to install [Rancher Desktop](https://rancherdesktop.io/), a free alternative to Docker Desktop.

1. Pull the image from the GitLab registry:

   ```shell
   docker pull registry.gitlab.com/mikeeddington/gitlab-sec-triage-helper:1
   docker tag registry.gitlab.com/mikeeddington/gitlab-sec-triage-helper:1 gl-sec-triage-helper
   ```

1. Run the command. You might consider creating a short shell script to make editing the arguments easier. **Note** The `--close_linked_issues` argument defaults to `true`.

   ```shell
   docker run --rm gl-sec-triage-helper \
       --ci_server_url https://gitlab.com \
       --ci_project_id 12345 \
       --ci_project_path mikeeddington/gitlab-sec-triage-helper \
       --filter-image registry.gitlab.com/mikeeddington/gitlab-sec-triage-helper:1-fips \
       --token XXXXXX \
      dismissed_to_resolved
   ```
