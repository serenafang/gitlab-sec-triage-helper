# syntax=docker/dockerfile:experimental

# File: Dockerfile
# Build:
#     DOCKER_BUILDKIT=1 docker build -t sometag -f Dockerfile $(pwd)

FROM python:3.10-alpine as devel

RUN \
    addgroup -S app && adduser -S -G app app \
    && apk -U upgrade --no-cache \
    && apk add --no-cache \
        curl \
        py3-wheel \
        py3-requests \
        py3-urllib3 \
        py3-jinja2 \
        py3-click

RUN curl -sSL https://install.python-poetry.org | python3 -

WORKDIR /app
COPY . /app/

RUN \
    python3 -m pip install poetry \
    && poetry build

# #########################################################

FROM python:3.10-alpine

RUN \
    addgroup -S app && adduser -S -G app app \
    && apk -U upgrade --no-cache \
    && apk add --no-cache \
        py3-wheel \
        py3-requests \
        py3-urllib3 \
        py3-jinja2 \
        py3-click

RUN --mount=from=devel,src=/app/dist,dst=/app/dist \
    python3 -m pip --no-cache-dir install /app/dist/*.whl

USER app
WORKDIR /home/app
ENTRYPOINT ["gl-sec-triage-helper"]
